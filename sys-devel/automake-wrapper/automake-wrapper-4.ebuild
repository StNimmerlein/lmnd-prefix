# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ./gentoo-x86-cvsroot/sys-devel/automake-wrapper/automake-wrapper-4.ebuild,v 1.9 2010/05/31 19:18:14 josejx Exp $

inherit multilib

DESCRIPTION="wrapper for automake to manage multiple automake versions"
HOMEPAGE="http://www.gentoo.org/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 m68k ~mips ppc ppc64 s390 sh sparc x86 ~ppc-aix ~sparc-fbsd ~x86-fbsd ~x64-freebsd ~x86-freebsd ~hppa-hpux ~ia64-hpux ~x86-interix ~amd64-linux ~ia64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE=""

S=${WORKDIR}

src_install() {
	exeinto /usr/$(get_libdir)/misc
	newexe "${FILESDIR}"/am-wrapper-${PV}.sh am-wrapper.sh || die

	keepdir /usr/share/aclocal

	dodir /usr/bin
	local x=
	for x in aclocal automake ; do
		dosym ../$(get_libdir)/misc/am-wrapper.sh /usr/bin/${x}
	done
}

pkg_postinst() {
	# added for prefix
	# the wrapper wants to find each version as a suffix to the name
	# it is called by
	for x in /usr/bin/{aclocal,automake}*; do
		if [ -f $x ]; then
			target=$(basename ${x})
			verline=$(${x} --version|head -n1)
			version=${verline//[^0-9.]}
			name=${target//[0-9.-]}
			if [ ! -e "${EPREFIX}/bin/$name-$version" ]; then
				einfo "Linking $name-$version to ${x}"
				ln -sf "${x}" "${EPREFIX}/bin/$name-$version"
			fi
		fi
	done
}
