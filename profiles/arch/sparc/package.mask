# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# Ben de Groot <yngwin@gentoo.org> (14 Jun 2012)
# Since x11-libs/qt-* will no longer be available for sparc, we need
# to mask the reverse dependencies
app-crypt/qca
app-crypt/qca-cyrus-sasl
app-crypt/qca-gnupg
app-crypt/qca-logger
app-crypt/qca-ossl
app-office/lyx
app-office/scribus
dev-db/tora
dev-embedded/qvfb
dev-libs/quazip
dev-python/PyQt4
dev-python/qscintilla-python
dev-util/automoc
dev-util/kscope
dev-vcs/qbzr
games-emulation/dboxfe
media-gfx/pictureflow
media-libs/phonon
net-irc/kvirc
sys-devel/qconf
x11-libs/qscintilla
x11-libs/qwt
x11-themes/qtcurve-qt4

# Justin Lecher <jlec@gentoo.org> (9 Mar 2011)
# sci-libs/plplot needs keywords #358035
=sci-biology/emboss-6.3.1*
