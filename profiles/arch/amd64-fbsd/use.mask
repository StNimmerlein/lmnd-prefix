# Copyright 1999-2012 Gentoo Foundation.
# Distributed under the terms of the GNU General Public License, v2
# $Header: /data/temp/gentoo//vcs-public-cvsroot/gentoo-x86/profiles/arch/amd64-fbsd/use.mask,v 1.7 2012/05/16 15:24:11 aballier Exp $

# Unmask the flag which corresponds to ARCH.
-amd64-fbsd

-3dnow
-3dnowext
-mmx
-mmxext
-sse
-sse2
-sse3
-sse4
-sse4a
-sse5
-ssse3
-win32codecs

# nvidia drivers works on x86
-xvmc
-nvidia
-video_cards_nvidia

# Unmask vmware drivers
#-video_cards_vmware
-input_devices_vmmouse

# Diablo Latte and Caffe (JRE and JDK) works here
-java
