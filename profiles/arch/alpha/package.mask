# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# Tobias Klausman <klausman@gentoo.org> (31 Oct 2011)
# Breaks since accept4() is not implemented on alpha
>=sys-fs/udev-167

# Alexey Shvetsov <alexxy@gentoo.org> (21 May 2012)
# Need deps pecl-apc and pecl-uploadprogress bug #416897
>=www-apps/drupal-7.14
