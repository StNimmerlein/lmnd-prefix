#!@GENTOO_PORTAGE_EPREFIX@/bin/bash
# Copyright 2006-2010 Gentoo Foundation; Distributed under the GPL v2
# $Id$

# Fabian Groffen <grobian@gentoo.org> -- 2007-03-10
# Enters the prefix environment by starting a login shell from the
# prefix.  The SHELL environment variable is elevated in order to make
# applications that start login shells to work, such as `screen`.

# if you come from a substantially polluted environment (another
# Prefix), calling this script as follows resolves most oddities I've
# ever seen:
# env -i HOME=$HOME TERM=$TERM USER=$USER /path/to/startprefix


# What is our prefix?
EPREFIX="@GENTOO_PORTAGE_EPREFIX@"

if [[ ${SHELL#${EPREFIX}} != ${SHELL} ]] ; then
	echo "You appear to be in prefix already (SHELL=$SHELL)" > /dev/stderr
	exit -1
fi

# not all systems have the same location for shells, however what it
# boils down to, is that we need to know what the shell is, and then we
# can find it in the bin dir of our prefix
SHELL=${SHELL##*/}
# set the prefix shell in the environment
export SHELL=${EPREFIX}/bin/${SHELL}
# check if the shell exists
if [[ ! -x $SHELL ]] ; then
	echo "Failed to find the Prefix shell, this is probably" > /dev/stderr
	echo "because you didn't emerge the shell ${SHELL##*/}" > /dev/stderr
	exit -1
fi

# unset variables that are known to cause trouble
unset PKG_CONFIG_PATH  # bug #234156

# give a small notice
echo "Entering lmonade prefix ${EPREFIX}"
# start the login shell
$SHELL -l
# and leave a message when we exit... the shell might return non-zero
# without having real problems, so don't send alarming messages about
# that
echo "Leaving lmonade prefix with exit status $?"
